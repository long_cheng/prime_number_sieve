#!/usr/bin/env kscript

val max = 10000
val range = 1..max
var numbers = range.toList()
var prime = 2
while (true) {
    println(prime)
    numbers = numbers.filter { it % prime != 0 }
    try {
        prime = numbers.first { it > prime }
    } catch (e: NoSuchElementException) {
        break
    }
}
