/* prime.kt
 *     Sieve of Eratosthenes algorithm implemented in Kotlin.
 *
 * Reference:
 *     https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
 * 
 * Command to compile:
 *     kotlinc prime_gen.kt -include-runtime -d prime_gen_kt.jar
 */ 

fun main(args: Array<String>) {
	fun make_sieve(src: Iterator<Int>, prime: Int) = iterator {
		for (n in src) {
			if (n % prime != 0) yield(n)
		}
	}
	var sieve = iterator {
		var x = 2
		while (true) yield(x++)
	}
	for (i in 1..10000) {
		val prime = sieve.next()
		println(prime)
		sieve = make_sieve(sieve, prime)
	}
}
