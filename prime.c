/* prime.c
 *     Sieve of Eratosthenes algorithm implemented in C.
 *
 * Reference:
 *     https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
 * 
 * Command to compile:
 *     clang -fblocks  -o prime_c prime.c
 */ 

#include <stdio.h>
#include <Block.h>
typedef int (^Sieve)();

Sieve make_sieve(Sieve src, int prime) {
	return Block_copy( ^(void) {
		int next;
		for (next = src(); next % prime == 0; next = src()) {}
		return next;
	});
}

Sieve make_numbers() {
	__block int i = 2;
	return Block_copy( ^(void) {
		return i++;
	});
}

int main(void) {
	Sieve sieves[10000];
	Sieve sieve = make_numbers();
	int prime;
	for (int i = 0; i < 10000; i++) { 
		sieves[i] = sieve;
		prime = sieve();
		printf("%d\n", prime);
		sieve = make_sieve(sieve, prime);
	}
	
	for (int i = 0; i < 10000; i++) { 
		Block_release(sieves[i]);
	}
	return 0;
}
