#!/usr/bin/env kscript

fun make_sieve(src: Iterator<Int>, prime: Int) = iterator {
	for (n in src) {
		if (n % prime != 0) yield(n)
	}
}

var sieve = iterator {
	var x = 2
	while (true) yield(x++)
}

for (i in 1..100) {
	val prime = sieve.next()
	println(prime)
	sieve = make_sieve(sieve, prime)
}
