/* prime.kt
 *     Sieve of Eratosthenes algorithm implemented in Kotlin.
 *
 * Reference:
 *     https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
 * 
 * Command to compile:
 *     kotlinc prime.kt -include-runtime -d prime_kt.jar
 */ 

fun main(args: Array<String>) {
	fun make_sieve(src: Sequence<Int>, prime: Int) = src.filter { it % prime != 0 }
	var sieve = sequence {
		var x = 2
		while (true) yield(x++)
	}
	for (i in 1..10000) {
		val prime = sieve.first()
		println(prime)
		sieve = make_sieve(sieve, prime)
	}
}
