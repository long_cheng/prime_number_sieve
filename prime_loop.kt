/* prime.kt
 *     Sieve of Eratosthenes algorithm implemented in Kotlin.
 *
 * Reference:
 *     https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
 * 
 * Command to compile:
 *     kotlinc prime_loop.kt -include-runtime -d prime_kt.jar
 */ 

fun main(args: Array<String>) {
	val max = 10000
	val range = 1..max
	var numbers = range.toList()
	var prime = 2
	while (true) {
		println(prime)
		numbers = numbers.filter { it % prime != 0 }
		try {
			prime = numbers.first { it > prime }
		} catch (e: NoSuchElementException) {
			break
		}
	}
}
