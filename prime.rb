=begin
prime.rb
    Sieve of Eratosthenes algorithm implemented in Ruby. 

Reference:
    https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
=end

def make_sieve(src, prime)
  Enumerator.new do |y|
    loop do
      next_prime = src.next
      y << next_prime if next_prime % prime != 0
    end	
  end
end

def primes
  sieve = (2..Float::INFINITY).lazy
  Enumerator.new do |y|
	loop do
	  prime  = sieve.next
	  y << prime
	  sieve = make_sieve(sieve, prime)
	end
  end
end

primes.lazy.take(10000).each {|n| puts n}
