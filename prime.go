/* prime.go
 *     Sieve of Eratosthenes algorithm implemented in Go.
 *
 * Reference:
 *     https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
 *
 * Command to compile:
 *     go build -o prime_go prime.go
 */
package main

func Source(ch chan<- int) {
	for i := 2; ; i++ {
		ch <- i
	}
}

func Sieve(src <-chan int, dst chan<- int, prime int) {
	for {
		i := <-src
		if i%prime != 0 {
			dst <- i
		}
	}
}

func main() {
	ch := make(chan int)
	go Source(ch)
	limit := 10000
	for {
		prime := <-ch
		println(prime)
		ch1 := make(chan int)
		go Sieve(ch, ch1, prime)
		ch = ch1
		limit -= 1
		if limit == 0 {
			break
		}
	}
}
