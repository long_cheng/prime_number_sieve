'''
prime.py
    Sieve of Eratosthenes algorithm implemented in Python.

Reference:
    https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
''' 

def make_sieve(src, number):
    return (n for n in src if n % number)

def source(first=1):
    while True:
        yield first
        first += 1

def primes():
    sieve = source(2)
    while True:
        prime = next(sieve)
        yield prime
        sieve = make_sieve(sieve, prime)

import sys
sys.setrecursionlimit(10**6)

import itertools
ten_thousand_primes = itertools.islice(primes(), 10000)
for n in ten_thousand_primes: print(n)
